const express = require('express')
const userController = require('./../controller/userController')
const authController = require('./../controller/authController')
const userCache = require('./../controller/cacheController')

const router = express.Router()

router.post('/token', authController.generateToken)

router.use(authController.protect)
router.route('/')
    .get(userCache.getAllUserCache, userController.getAllUser)
    .post(userController.createUser)

router.route('/:id')
    .get(userCache.getUserCache, userController.getUser)
    .patch(userController.updateUser)
    .delete(userController.deleteUser)

module.exports = router