const mongoose = require('mongoose')
const validator = require('validator')

const userSchema = new mongoose.Schema({
    userName: {
        type: String,
        required: [true, 'A User must have username'],
        unique: true,
        // default: hehe
    },
    accountNumber: {
        type: Number,
        required: [true, 'A User must have Account Number']
    },
    emailAddress: {
        type: String,
        required: [true, 'A User must have email'],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, 'Please provide a valid email']
    },
    identityNumber: {
        type: Number,
        required: [true, 'A User must have Identity Number'],
        minlength: 8
    },
    createdAt: {
        type: Date,
        default: Date.now(),
        select: false
    }
})

// testUser.save().then(doc => {
//     console.log(doc)
// }).catch(err => {
//     console.log('Error 🔥: ', err)
// })

const User = mongoose.model('User', userSchema)

module.exports = User