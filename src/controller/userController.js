const User = require('./../models/userModel')
const cacheController = require('./cacheController')

const getAllUser = async (req, res) => {
    try {
        const queryObj = {...req.query}

        const users = await User.find(queryObj)

        // set cache all user
        cacheController.setCache('redis_prasetyotrimuktisetiko_betest', users)
    
        res.status(200).json({
            status: 'success',
            results: users.length,
            data: {
                users
            }
        })
    } catch (error) {
        res.status(400).json({
            status: 'failed',
            message: error.message
        })
    }

}

const createUser = async (req, res) => {
    try {
        const newUser = await User.create(req.body)
        const users = await User.find({})

        // set cache new user
        cacheController.setCache('redis_prasetyotrimuktisetiko_betest', users)
    
        res.status(200).json({
            status: 'success',
            data: {
                user: newUser
            }
        })
    } catch (error) {
        res.status(400).json({
            status: 'failed',
            message: error.message
        })
    }

}

const getUser = async (req, res) => {
    try {
        const user = await User.findById(req.params.id)

        // set cache user id
        cacheController.setCache(req.params.id, user)
    
        res.status(200).json({
            status: 'success',
            data: {
                user
            }
        })
    } catch (error) {
        res.status(400).json({
            status: 'failed',
            message: error.message
        })
    }

}

const updateUser = async (req, res) => {
    try {
        const user = await User.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true
        })

        // set cache after update
        cacheController.setCache(req.params.id, user)
    
        res.status(200).json({
            status: 'success',
            data: {
                user
            }
        })
    } catch (error) {
        res.status(400).json({
            status: 'failed',
            message: error.message
        })
    }

}

const deleteUser = async (req, res) => {
    try{
        await User.findByIdAndDelete(req.params.id)

        res.status(200).json({
            status: 'success',
            message: 'User has been deleted'
        })
    }catch(error){
        res.status(400).json({
            status: 'failed',
            message: error.message
        })
    }
}

module.exports = {getAllUser, createUser, getUser, updateUser, deleteUser}