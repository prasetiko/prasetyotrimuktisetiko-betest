const jwt = require('jsonwebtoken')
const { promisify } = require('util')

const generateToken = async (req, res) => {
    try {
        const token = jwt.sign({id: 1234567890}, process.env.JWT_SECRET, {
            expiresIn: process.env.JWT_EXPIRES_IN
        })
    
        res.status(200).json({
            status: 'success',
            token
        })
    }catch (err){
        res.status(400).json({
            status: 'failed',
            message: 'Generate token failed'
        })
    }
}

const protect = async (req, res, next) => {
    try {
        let token
        if (req.headers.authorization && req.headers.authorization.split('Bearer')){
            token = req.headers.authorization.split(' ')[1]
        }
    
        if (!token){
            return res.status(401).json({
                status: 'failed',
                message: 'Please generate Token first'
            })
        }
    
        await promisify(jwt.verify)(token, process.env.JWT_SECRET)
        next()

    }catch (err){
        res.status(400).json({
            status: 'failed',
            message: err.message
        })
    }
}

module.exports = {generateToken, protect}