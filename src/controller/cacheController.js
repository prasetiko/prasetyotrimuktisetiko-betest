const Redis = require('ioredis')
const redis = new Redis()

// const client = redis.createClient(process.env.REDIS_PORT);

//log error to the console if any occurs
redis.on("error", (err) => {
    console.log(err);
});

const getAllUserCache = (req, res, next) => {
    redis.get('redis_prasetyotrimuktisetiko_betest', (error, result) => {
        const users = JSON.parse(result)
        if(error) throw error
        if(result !== null){
            return res.status(200).json({
                status: 'success',
                results: users.length,
                data: {
                    users
                }
            })
        }else{
            return next ()
        }
    })
}

const getUserCache = (req, res, next) => {
    redis.get(req.params.id, (error, result) => {
        if (error) throw error;
        if (result !== null) {
            return res.status(200).json({
                status: 'success',
                data: {
                    user: JSON.parse(result)
                }
            })
        } else {
            return next();
        }
    })
}

const setCache = (key, val) => {
    redis.set(key, JSON.stringify(val));
}

module.exports = {getUserCache, setCache, getAllUserCache}