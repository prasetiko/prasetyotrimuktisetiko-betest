const express = require('express')
const dotenv = require('dotenv')
const morgan = require('morgan')
const mongoose = require('mongoose')
const userRouter = require('./src/routes/userRoutes')

dotenv.config({ path: './config.env' });
const app = express()
app.use(morgan('dev'))
app.use(express.json())

const DB = process.env.DATABASE_LOCAL

mongoose.connect(DB, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
}).then(() => console.log('Db Connenction successfull'))

app.use('/api/v1/users', userRouter)

const port = process.env.PORT || 8080
app.listen(port, () => {
    console.log(`Running on port ${port}...`)
})